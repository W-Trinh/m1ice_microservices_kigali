from flask import Flask, jsonify

app = Flask(__name__)

# Configuration pour utiliser l'UTF-8 plutôt que l'unicode
app.config['JSON_AS_ASCII'] = False


# Endpoint pour le ping
@app.route('/Info/ping')
def ping():
  ping_data =  "OK"
  return jsonify(ping_data), 200


# Endpoint pour obtenir la version de build
@app.route('/Info/buildVersion')
def build_version():
    version_data = {"version":"1.0"}
    return jsonify(version_data), 200


# Endpoint pour obtenir l'horodatage de build
@app.route('/Info/buildTimestamp')
def build_timestamp():
    timeData = {"time_data":"20git23-06-06T07:53:52.043Z"}
    return jsonify(timeData), 200


# Point d'entrée de l'application Flask
if __name__ == '__main__':
    # Lance l'application Flask
    app.run(host='0.0.0.0', port=5003)
