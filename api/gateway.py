from flask import Flask, jsonify, request
import requests

app = Flask(__name__)

@app.route('/api', methods=['GET'])
def welcome():
    return jsonify('Welcome to our Gateway'), 500

# Endpoint de la passerelle API
@app.route('/api/<path:chemin>', methods=['GET'])
def gateway(chemin):
    # Détermine le microservice cible en fonction du chemin de l'URL
    if chemin.startswith('air'):
        service_url = 'http://api-air:5001'
    elif chemin.startswith('Camera/takePicture'):
        service_url = 'http://api-camera:5002'
    elif chemin.startswith('Info/buildTimestamp') or chemin.startswith('/Info/buildVersion') or chemin.startswith('/Info/ping'):
        service_url = 'http://api-info:5003'
    elif chemin.startswith('gps') or chemin.startswith('compass'):
        service_url = 'http://api-localisation:5004'
    elif chemin.startswith('pressure') or chemin.startswith('temperature') or chemin.startswith('oceanographicParameters'):
        service_url = 'http://api-ocean:5005'
    else:
        return jsonify({'error': 'API not found'})

    # Récupère les données de la requête entrante
    data = request.get_data()

    try:
        # Effectue une requête vers le microservice cible
        response = requests.request(
            method=request.method,
            url=f'{service_url}/{chemin}',
            headers=request.headers,
            data=data
        )

        # Renvoie la réponse du microservice
        return jsonify(response.json()), response.status_code

    # En cas d'erreur de requête
    except requests.exceptions.JSONDecodeError:
        print(response.content) 
        return jsonify({'error': 'JSON Decode Error'}), 500
    
    
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)