from flask import Flask, jsonify

app = Flask(__name__)

# Configuration pour utiliser l'UTF-8 plutôt que l'unicode
app.config['JSON_AS_ASCII'] = False

# Route racine pour afficher les liens vers les ressources
@app.route('/')
def welcome():
    links = [
        {
            "_links": [
                {
                    "href": "/gps",
                    "rel": "gps"
                },
                {
                    "href": "/compass",
                    "rel": "compass"
                }
            ]
        }
    ]
    return jsonify(links), 200

# Définition de la route /compass pour la méthode GET
@app.route('/compass', methods=['GET'])
def get_compass():
    # Données de compass à retourner
    compass_data = {
        "dateTime": "2023-06-06T09:55:27.024Z",
        "direction": 0
    }
    
    # Retourne les données sous forme de réponse JSON avec le code de statut 200 (OK)
    return jsonify(compass_data), 200

# Définition de la route /gps pour la méthode GET
@app.route('/gps', methods=['GET'])
def get_gps():
    # Données de compass à retourner
    gps_data = {
        "dateTime": "2023-06-06T09:55:27.013Z",
        "position": {
            "latitude": 0,
            "longitude": 0,
            "altitude": 0
        }
    }


    # Return the data as a JSON response with status code 200 (OK)
    return jsonify(gps_data), 200


# Point d'entrée de l'application Flask
if __name__ == '__main__':
    # Lance l'application Flask
    app.run(host='0.0.0.0', port=5003)
