from flask import Flask, jsonify

app = Flask(__name__)


app.config['JSON_AS_ASCII'] = False

#acceuil de l'API listant l'ensemble des liens de l'API
@app.route("/")
def welcome():
    links = [
        {
            "_links": [
                {
                    "href": "/pressure",
                    "rel": "pressure"
                },
                {
                    "href": "/temperature",
                    "rel": "temperature"
                },
                {
                    "href": "/oceanographicParameters",
                    "rel": "oceanographicParameters"
                }
            ]
        }
    ]
    return jsonify(links), 200

#route retournant la pression
@app.route("/pressure",methods=["GET"])
def getPressure():
    pressure = {"pressure":10}
    return jsonify(pressure), 200

#route retournant la température
@app.route("/temperature",methods=["GET"])
def getTemperature():
   temperature = {"temperature":25}
   return jsonify(temperature),200

#route retournant les paramètres océaniques
@app.route("/oceanographicParameters",methods=["GET"])
def getOceanicParameters():
    oceanic = [{"saltLevel": 35}]
    return jsonify(oceanic),200


# Point d'entrée de l'application Flask
if __name__ == '__main__':
    # Lance l'application Flask
    app.run(host='0.0.0.0', port=5005)
