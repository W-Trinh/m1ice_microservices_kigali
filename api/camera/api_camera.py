from flask import Flask, jsonify

app = Flask(__name__)

# Configuration to use UTF-8 instead of Unicode
app.config['JSON_AS_ASCII'] = False


# Route racine pour afficher le lien vers la ressource
@app.route('/')
def welcome():
    link = [
        {
            "_links": [
                {
                    "href": "/Camera/takePicture",
                    "rel": "Camera/takePicture"
                }
            ]
        }
    ]
    return jsonify(link), 200


# Endpoint to take a picture
@app.route('/Camera/takePicture', methods=['GET'])
def take_picture():
    # Picture data to return

    pictureURL = "http://example.com/pictures/123.jpg"
    picture_data = {
        "dateTime": "2023-06-08T06:46:06.805Z",
        "pictureURL": pictureURL
    }
    
    # Return the data as a JSON response with status code 200 (OK)
    return jsonify(picture_data), 200


# Point d'entrée de l'application Flask
if __name__ == '__main__':
    # Lance l'application Flask
    app.run(host='0.0.0.0', port=5002)
