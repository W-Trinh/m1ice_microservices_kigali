from flask import Flask, jsonify

app = Flask(__name__)

# Configuration pour utiliser l'UTF-8 plutôt que l'unicode
app.config['JSON_AS_ASCII'] = False

# Route racine pour afficher le lien vers la ressource
@app.route('/')
def welcome():
    link = [
        {
            "_links": [
                {
                    "href": "/air",
                    "rel": "air"
                }
            ]
        }
    ]
    return jsonify(link), 200

# Définition de la route /air pour la méthode GET
@app.route('/air', methods=['GET'])
def get_air():
    # Données de l'air à retourner
    air_data = {
        "dateTime": "2023-06-06T07:53:52.043Z",
        "airLevel": 0
    }
    
    # Retourne les données sous forme de réponse JSON avec le code de statut 200 (OK)
    return jsonify(air_data), 200

# Point d'entrée de l'application Flask
if __name__ == '__main__':
    # Lance l'application Flask
    app.run(host='0.0.0.0', port=5001)
